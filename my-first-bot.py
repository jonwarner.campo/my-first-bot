###########################Python Import Libraries##############################

import json, requests
import time
import sys, getopt
import os
from pprint import pprint
from webexteamssdk import WebexTeamsAPI
from flask import Flask, request, render_template, url_for

###########################Cisco Webex Teams Header##############################

BOT_SPARK_TOKEN="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
bot_email = 'XXXXXXXXXXXXXXXXXXXX@webex.bot'
Spark_Base_URL = "https://api.ciscospark.com/v1"


########################Cisco Webex (Former Spark) URL join####################################
def _spark_api(noun):
    return ''.join(('https://api.ciscospark.com/v1/', noun))

########################Cisco Webex (Former Spark) message header####################################
def _headers(bot_token):
    return {'Content-type': 'application/json',
            'Authorization': 'Bearer ' + bot_token}

########################Cisco Webex (Former Spark) get resource####################################
def send_spark_get(end_url, payload=None, js=True):
    if payload == None:
        request = requests.get(_spark_api(end_url), headers=_headers(BOT_SPARK_TOKEN))
    else:
        request = requests.get(_spark_api(end_url), headers=_headers(BOT_SPARK_TOKEN), params=payload)
    if js == True:
        request = request.json()

    return request

########################Cisco Webex (Former Spark) create message####################################

def ciscospark(message, toemail):
    bearer = BOT_SPARK_TOKEN
    email = toemail
    body = {"toPersonEmail": email, "markdown": message}
    headers = {"Content-type": "application/json", "Authorization": "Bearer " + bearer}
    response = requests.post(url="https://api.ciscospark.com/v1/messages",json=body, headers=headers)
    print(response.status_code)

###################################Message Handling#######################################

def handle_text(message):
    message = message.lower()
    result = "Resource not found"
    if "bad ip assignment" in message:
        result = "Cause: Device cannot get an ARP reply from its default gateway, it will revert back to using DHCP"\
                 "\n\nhttps://documentation.meraki.com/zGeneral_Administration/Cross-Platform_Content/Bad_IP_Assignment_Configuration"

    elif "wireless packet capture" in message:
        result = "Wireless packet capture - https://documentation.meraki.com/MR/Monitoring_and_Reporting/Capturing_Wireless_Traffic_from_a_Client_Machine"\
        "\nAnalyzing Wireless packet capture - https://documentation.meraki.com/zGeneral_Administration/Tools_and_Troubleshooting/Analyzing_Wireless_Packet_Captures"

    elif "client vpn" in message:
        result = "Troubleshooting Client VPN - https://documentation.meraki.com/MX/Client_VPN/Troubleshooting_Client_VPN"\
        "\n\nClient VPN OS Configuration - https://documentation.meraki.com/MX/Client_VPN/Client_VPN_OS_Configuration"\
        "\n\nTry this basic troubleshooting steps:"\
        "\n1. Packet capture and check if UDP ports 500 and 4500 are present"\
        "\n2. Try using Meraki Auth, and login using test phone and credential. If works, possible device troubleshooting"\
        "\n3. Check Device Error codes"

    elif "list clients" in message:
        url = "https://api-mp.meraki.com/api/v1/networks/N_649081296294793886/clients"
        payload = {}
        headers = { 'X-Cisco-Meraki-API-Key': '34bfad2b70705f33c04a7e28df6b1ce67083ac18'
        }
        response = requests.request("GET", url, headers=headers, data=payload)
        # print(response.text.encode('utf8'))
        json_response = response.json()
        print(json_response)
        result = "\n\n".join(str(x) for x in json_response)

    elif "firewall" in message:
        message_list = list(message.split(" "))
        user_serial = message_list[1]
        print(user_serial)

        url = "https://api-mp.meraki.com/api/v1/organizations/319307/devices/statuses"

        payload = {}
        headers = {
            'X-Cisco-Meraki-API-Key': '98f71a2a80f96841200bac8f56dd57aad10cfe8d'
        }

        response = requests.request("GET", url, headers=headers, data=payload)

        json_response = response.json()

        network_id = ""
        for i in json_response:

            if user_serial == i['serial'].lower():
                network_id = i['networkId']

        url = "https://api-mp.meraki.com/api/v1/networks/" + network_id + "/appliance/firewall/l3FirewallRules"

        payload = {}
        headers = {
            'X-Cisco-Meraki-API-Key': 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        }

        response = requests.request("GET", url, headers=headers, data=payload)

        json_response = response.json()
        print(json_response)
        result = "\n\n".join(str(x) for x in json_response['rules'])


    elif "guest wifi" in message or "wifi guest" in message:
        message_list = list(message.split(" "))
        wifi_status = message_list[2]

        if wifi_status == "on":
            payload = "{\n    \"name\": \"XXXXXXXXXXXXXXXXwifi nameXXXXXXXXXXXXXXXXXX\",\n    \"enabled\": true\n}\n"
            result = "guest wifi is now on"
        elif wifi_status == "off":
            payload = "{\n    \"name\": \"XXXXXXXXXXXXXXwifi nameXXXXXXXXXXXX\",\n    \"enabled\": false\n}\n"
            result = "guest wifi is now off"
        else:
            result = "wrong syntax"


        import requests

        url = "https://api-mp.meraki.com/api/v1/networks/L_683984193406899227/wireless/ssids/2"


        headers = {
            'Content-Type': 'application/json',
            'X-Cisco-Meraki-API-Key': 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        }

        response = requests.request("PUT", url, headers=headers, data=payload)

        print(response.text.encode('utf8'))

    return result


###########################Web Flask App for Running Web service and Waiting for GET and POST request###########################
app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def spark_webhook():
    if request.method == 'POST':
        webhook = request.get_json(silent=True)
        resource = webhook['resource']
        senders_email = webhook['data']['personEmail']
        room_id = webhook['data']['roomId']

        print(senders_email)
        #print(room_id)

        if senders_email != bot_email:
            result = send_spark_get('messages/{0}'.format(webhook['data']['id']))  ###gets message content based on data id of webhook data

            in_message = result["text"]

            result = handle_text(in_message)
            ciscospark(result, senders_email)

        return 'ok'


    elif request.method == 'GET':
        message = "<center><img src=\"http://bit.ly/SparkBot-512x512\" alt=\"Spark Bot\" style=\"width:256; height:256;\"</center>" \
                  "<center><h2><b>Congratulations! Your <i style=\"color:#ff8000;\">%s</i> bot is up and running.</b></h2></center>" \
                  "<center><b><i>Please don't forget to create Webhooks to start receiving events from Cisco Spark!</i></b></center>"

        return message, 200


if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
